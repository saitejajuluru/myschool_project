from django.contrib import admin
from crud_app.models import grade,section,subject,student,studentlogin

class gradeAdmin(admin.ModelAdmin):
    list_display = ['section','gradename']
admin.site.register(grade,gradeAdmin)
class sectionAdmin(admin.ModelAdmin):
    list_display = ['sectionname']
admin.site.register(section,sectionAdmin)
class subjectAdmin(admin.ModelAdmin):
    list_display = ['grade','subjectname']
admin.site.register(subject,subjectAdmin)

class studentAdmin(admin.ModelAdmin):
    list_display = ['Name','Enrollmentnumber','studentgrade','sec','email']
    list_display_links = ['Name','Enrollmentnumber']
admin.site.register(student,studentAdmin)
class studentloginAdmin(admin.ModelAdmin):
    list_display = ['sname']
admin.site.register(studentlogin,studentloginAdmin)
