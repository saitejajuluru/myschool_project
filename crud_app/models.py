from django.db import models
from django.db.models.deletion import CASCADE

class section(models.Model):
    sectionname=models.CharField(max_length=20)

    def __str__(self):
        return self.sectionname

class grade(models.Model):
    section=models.ForeignKey(section,on_delete=CASCADE,related_name="section")
    gradename=models.CharField(max_length=20)

    def __str__(self):
        return self.gradename

class subject(models.Model):
    grade=models.ForeignKey(grade,on_delete=CASCADE,related_name="grade")
    subjectname=models.CharField(max_length=20)

    def __str__(self):
        return self.subjectname
class student(models.Model):
    Name=models.CharField(max_length=50)
    Enrollmentnumber=models.IntegerField(unique=True)
    studentgrade=models.CharField(max_length=20)
    sec=models.CharField(max_length=20)
    email=models.EmailField(max_length=40)
    phone=models.IntegerField()
    profilepic=models.FileField()

    def __str__(self):
        return self.Name


class studentlogin(models.Model):
    sname=models.CharField(max_length=30)
    spw=models.SlugField()

    def __str__(self):
        return self.sname

