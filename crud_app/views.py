from django.shortcuts import render,redirect
from crud_app.models import student
from crud_app.forms import studentloginForm
def home(request):
    form=studentloginForm()
    return render(request,'home.html',{'form':form})
def login(request):
    if request.method == "POST":
        myform=studentloginForm(request.POST)
        if myform.is_valid():
            sname=myform.cleaned_data['sname']
            stu=student.objects.all()
            return render(request, "login.html", {'stu': stu})

    else:
        myform=studentloginForm()
        return redirect('/home')

